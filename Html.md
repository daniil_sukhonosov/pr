HTML que permet escollir la informació que es vol veure del ordinador i envia a un php resultant que mostra la informació:

```
<link href="Part3.css" rel="stylesheet" type="text/css">
<html>
<body>
<h1>Informació del Ordinador</h1>
<h3>Determina quina informació vols saber</h3>
<form method="GET" action="Part3.php">
        <select name="info">
                <option value="CPU">CPU</option>
                <option value="System">System</option>
                <option value="Xarxa">Xarxa</option>
                <option value="Disc">Disc</option>
                <option value="Memoria">Memoria</option>
        </select>
        <br><input type="submit" name="taula" class="boton" id="boton" value="Escollir">
</form>
</body>
</html>
```
