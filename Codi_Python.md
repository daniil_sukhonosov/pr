#Codi Python

Codi python que recopila informació sobre l'ordinador i ho envia a una base de dades:

```
from __future__ import print_function
from datetime import date, datetime, timedelta
import mysql.connector
import psutil
import platform
import time
from datetime import datetime
import time
mydb = mysql.connector.connect(
host="127.0.0.1",
user="CPU",
password="asix",
database="CPUinfo")
def ejecutaScript():
    def get_size(bytes, suffix="B"):
        """
        Scale bytes to its proper format
        e.g:
            1253656 => '1.20MB'
            1253656678 => '1.17GB'
        """
        factor = 1024
        for unit in ["", "K", "M", "G", "T", "P"]:
            if bytes < factor:
                return f"{bytes:.2f}{unit}{suffix}"
            bytes /= factor
            
    print("="*40, "System Information", "="*40)
    uname = platform.uname()
    s1 = uname.system
    print(f"System: ",s1)
    s2 = uname.node
    print(f"Node Name: ",s2)
    s3 = uname.release
    print(f"Release: ",s3)
    s4 = uname.version
    print(f"Version: ",s4)
    s5 = uname.machine
    print(f"Machine: ",s5)
    s6 = uname.processor
    print(f"Processor: ",s6)
    print("="*40, "Boot Time", "="*40)
    boot_time_timestamp = psutil.boot_time()
    bt = datetime.fromtimestamp(boot_time_timestamp)
    print(f"Boot Time: {bt.year}/{bt.month}/{bt.day} {bt.hour}:{bt.minute}:{bt.second}")
    print("="*40, "CPU Info", "="*40)
    print("Physical cores:", psutil.cpu_count(logical=False))
    print("Total cores:", psutil.cpu_count(logical=True))
    cpufreq = psutil.cpu_freq()
    print(f"Max Frequency: {cpufreq.max:.2f}Mhz")
    print(f"Min Frequency: {cpufreq.min:.2f}Mhz")
    print(f"Current Frequency: {cpufreq.current:.2f}Mhz")
    print("CPU Usage Per Core:")
    for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
        print(f"Core {i}: {percentage}%")
    print(f"Total CPU Usage: {psutil.cpu_percent()}%")
    print("="*40, "Memory Information", "="*40)
    svmem = psutil.virtual_memory()
    print(f"Total: {get_size(svmem.total)}")
    print(f"Available: {get_size(svmem.available)}")
    print(f"Used: {get_size(svmem.used)}")
    print(f"Percentage: {svmem.percent}%")
    print("="*20, "SWAP", "="*20)
    swap = psutil.swap_memory()
    print(f"Total: {get_size(swap.total)}")
    print(f"Free: {get_size(swap.free)}")
    print(f"Used: {get_size(swap.used)}")
    print(f"Percentage: {swap.percent}%")
    print("="*40, "Disk Information", "="*40)
    print("Partitions and Usage:")
    partitions = psutil.disk_partitions()
    for partition in partitions:
        print(f"=== Device: {partition.device} ===")
        print(f"  Mountpoint: {partition.mountpoint}")
        print(f"  File system type: {partition.fstype}")
        try:
            partition_usage = psutil.disk_usage(partition.mountpoint)
        except PermissionError:
            continue
        print(f"  Total Size: {get_size(partition_usage.total)}")
        print(f"  Used: {get_size(partition_usage.used)}")
        print(f"  Free: {get_size(partition_usage.free)}")
        print(f"  Percentage: {partition_usage.percent}%")
        mycursor = mydb.cursor()
        sql = ("INSERT INTO Informacio_Disc (Dispositiu,Punt_de_muntatge,File_system,Tamany_total,Utilitzat,Disponible,Percentatge) VALUES (%s,%s,%s,%s,%s,%s,%s)")
        val = (partition.device,partition.mountpoint,partition.fstype,get_size(partition_usage.total),get_size(partition_usage.used),get_size(partition_usage.free),partition_usage.percent)
        mycursor.execute(sql, val)
        mydb.commit()
    disk_io = psutil.disk_io_counters()
    print(f"Total read: {get_size(disk_io.read_bytes)}")
    print(f"Total write: {get_size(disk_io.write_bytes)}")
    print("="*40, "Network Information", "="*40)
    if_addrs = psutil.net_if_addrs()
    for interface_name, interface_addresses in if_addrs.items():
        for address in interface_addresses:
            print(f"=== Interface: {interface_name} ===")
            if str(address.family) == 'AddressFamily.AF_INET':
                print(f"  IP Address: {address.address}")
                print(f"  Netmask: {address.netmask}")
                print(f"  Broadcast IP: {address.broadcast}")
            elif str(address.family) == 'AddressFamily.AF_PACKET':
                print(f"  MAC Address: {address.address}")
                print(f"  Netmask: {address.netmask}")
                print(f"  Broadcast MAC: {address.broadcast}")
            mycursor = mydb.cursor()
            sql = ("INSERT INTO Informacio_Xarxa (Interficie,IP,Mascara,Broadcast_ip) VALUES (%s,%s,%s,%s)")
            val = (interface_name,address.address,address.netmask,address.broadcast)
            mycursor.execute(sql, val)
            mydb.commit()
    net_io = psutil.net_io_counters()
    print(f"Total Bytes Sent: {get_size(net_io.bytes_sent)}")
    print(f"Total Bytes Received: {get_size(net_io.bytes_recv)}")
    
    mycursor = mydb.cursor()
    sql = ("INSERT INTO Informacio_Sistema (Sistema,Node,Rel,Versio,Processador) VALUES (%s,%s,%s,%s,%s)")
    val = (s1,s2,s3,s4,s6)
    mycursor.execute(sql, val)
    mydb.commit()
    
    mycursor = mydb.cursor()
    sql = ("INSERT INTO Informacio_CPU (Cores_F,Cores_Totals,Frequencia_max,Frequencia_mix,Frequencia_actual,CPU_us_total) VALUES (%s,%s,%s,%s,%s,%s)")
    val = (psutil.cpu_count(logical=False),psutil.cpu_count(logical=True),cpufreq.max,cpufreq.min,cpufreq.current,psutil.cpu_percent())
    mycursor.execute(sql, val)
    mydb.commit()
    
    mycursor = mydb.cursor()
    sql = ("INSERT INTO Informacio_Memoria (Total,Disponible,En_us,Percentatge) VALUES (%s,%s,%s,%s)")
    val = (get_size(svmem.total),get_size(svmem.available),get_size(svmem.used),svmem.percent)
    mycursor.execute(sql, val)
    mydb.commit()

    time.sleep(10)

while True:
    ejecutaScript()
```
