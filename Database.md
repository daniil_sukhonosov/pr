Exportació de la base de dades:
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema CPUinfo
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema CPUinfo
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `CPUinfo` DEFAULT CHARACTER SET utf8 ;
USE `CPUinfo` ;

-- -----------------------------------------------------
-- Table `CPUinfo`.`CPU`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CPUinfo`.`CPU` (
  `idCPU` INT(11) NOT NULL,
  PRIMARY KEY (`idCPU`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `CPUinfo`.`Informacio_CPU`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CPUinfo`.`Informacio_CPU` (
  `Cores_F` VARCHAR(40) NULL DEFAULT NULL,
  `Cores_Totals` VARCHAR(40) NULL DEFAULT NULL,
  `Frequencia_max` VARCHAR(40) NULL DEFAULT NULL,
  `Frequencia_mix` VARCHAR(40) NULL DEFAULT NULL,
  `Frequencia_actual` VARCHAR(40) NULL DEFAULT NULL,
  `CPU_us_total` VARCHAR(40) NULL DEFAULT NULL,
  `CPU_idCPU` INT(11) NOT NULL,
  PRIMARY KEY (`CPU_idCPU`),
  CONSTRAINT `fk_Informacio_CPU_CPU`
    FOREIGN KEY (`CPU_idCPU`)
    REFERENCES `CPUinfo`.`CPU` (`idCPU`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `CPUinfo`.`Informacio_Disc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CPUinfo`.`Informacio_Disc` (
  `Dispositiu` VARCHAR(70) NULL DEFAULT NULL,
  `Punt_de_muntatge` VARCHAR(40) NULL DEFAULT NULL,
  `File_system` VARCHAR(40) NULL DEFAULT NULL,
  `Tamany_total` VARCHAR(40) NULL DEFAULT NULL,
  `Utilitzat` VARCHAR(40) NULL DEFAULT NULL,
  `Disponible` VARCHAR(40) NULL DEFAULT NULL,
  `Percentatge` VARCHAR(40) NULL DEFAULT NULL,
  `CPU_idCPU` INT(11) NOT NULL,
  PRIMARY KEY (`CPU_idCPU`),
  CONSTRAINT `fk_Informacio_Disc_CPU1`
    FOREIGN KEY (`CPU_idCPU`)
    REFERENCES `CPUinfo`.`CPU` (`idCPU`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `CPUinfo`.`Informacio_Memoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CPUinfo`.`Informacio_Memoria` (
  `Total` VARCHAR(40) NULL DEFAULT NULL,
  `Disponible` VARCHAR(40) NULL DEFAULT NULL,
  `En_us` VARCHAR(40) NULL DEFAULT NULL,
  `Percentatge` VARCHAR(40) NULL DEFAULT NULL,
  `CPU_idCPU` INT(11) NOT NULL,
  PRIMARY KEY (`CPU_idCPU`),
  CONSTRAINT `fk_Informacio_Memoria_CPU1`
    FOREIGN KEY (`CPU_idCPU`)
    REFERENCES `CPUinfo`.`CPU` (`idCPU`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `CPUinfo`.`Informacio_Sistema`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CPUinfo`.`Informacio_Sistema` (
  `Sistema` VARCHAR(40) NULL DEFAULT NULL,
  `Node` VARCHAR(40) NULL DEFAULT NULL,
  `Rel` VARCHAR(40) NULL DEFAULT NULL,
  `Versio` VARCHAR(40) NULL DEFAULT NULL,
  `Processador` VARCHAR(40) NULL DEFAULT NULL,
  `CPU_idCPU` INT(11) NOT NULL,
  PRIMARY KEY (`CPU_idCPU`),
  CONSTRAINT `fk_Informacio_Sistema_CPU1`
    FOREIGN KEY (`CPU_idCPU`)
    REFERENCES `CPUinfo`.`CPU` (`idCPU`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `CPUinfo`.`Informacio_Xarxa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CPUinfo`.`Informacio_Xarxa` (
  `Interficie` VARCHAR(40) NULL DEFAULT NULL,
  `IP` VARCHAR(40) NULL DEFAULT NULL,
  `Mascara` VARCHAR(40) NULL DEFAULT NULL,
  `Broadcast_ip` VARCHAR(40) NULL DEFAULT NULL,
  `CPU_idCPU` INT(11) NOT NULL,
  PRIMARY KEY (`CPU_idCPU`),
  CONSTRAINT `fk_Informacio_Xarxa_CPU1`
    FOREIGN KEY (`CPU_idCPU`)
    REFERENCES `CPUinfo`.`CPU` (`idCPU`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
