Codi php de la pàgina resultant:

```
<link href="Part3.css" rel="stylesheet" type="text/css">
<html>
<body>
<?php
$db_host="127.0.0.1";
$db_user="CPU";
$db_password="asix";
$db_name="CPUinfo";
$db_connection=mysqli_connect($db_host, $db_user, $db_password);
if (!$db_connection) {
	die('No hem pogut connectar amb la base de dades');
}
$cpu="Informacio_CPU";
$disc="Informacio_Disc";
$memoria="Informacio_Memoria";
$sistema="Informacio_Sistema";
$xarxa="Informacio_Xarxa";
$selectcpu="select * from " .$db_name. "." .$cpu. ";";
$selectdisc="select * from " .$db_name. "." .$disc. ";";
$selectmemoria="select * from " .$db_name. "." .$memoria. ";";
$selectsistema="select * from " .$db_name. "." .$sistema. ";";
$selectxarxa="select * from " .$db_name. "." .$xarxa. ";";
$info=$_GET['info'];
if ($info=='CPU') {
?>
<h1>Informació de la CPU</h1>
<div class="taula">
<table>
	<thead>
	<tr>
		<th>Cores Funcionals</th>
		<th>Cores Totals</th>
		<th>Freqüència màxima</th>
		<th>Freqüència mínima</th>
		<th>Freqüència actual</th>
		<th>Ús total de la CPU</th>
	</tr>
	</thead>
<?php foreach ($db_connection->query($selectcpu) as $row){ ?>
<tr>	
	<td><?php echo $row["Cores_F"]?></td>
	<td><?php echo $row["Cores_Totals"]?></td>
	<td><?php echo $row["Frequencia_max"]?></td>
	<td><?php echo $row["Frequencia_mix"]?></td>
	<td><?php echo $row["Frequencia_actual"]?></td>
	<td><?php echo $row["CPU_us_total"]?></td>
</tr>
<?php } ?>
</table>
</div>
<?php 
}
if ($info=='System') {
?>
<h1>Informació del sistema</h1>
<div class="taula">
<table>
        <thead>
        <tr>
                <th>Sistema Operatiu</th>
                <th>Nodes</th>
                <th>Release</th>
                <th>Versió</th>
                <th>Processador</th>
        </tr>
        </thead>
<?php foreach ($db_connection->query($selectsistema) as $row){ ?>
<tr>    
        <td><?php echo $row["Sistema"]?></td>
        <td><?php echo $row["Node"]?></td>
        <td><?php echo $row["Rel"]?></td>
        <td><?php echo $row["Versio"]?></td>
        <td><?php echo $row["Processador"]?></td>
</tr>
<?php } ?>
</table>
</div>
<?php 
}
if ($info=='Memoria') {
?>
<h1>Informació de la memoria</h1>
<div class="taula">
<table>
        <thead>
        <tr>
                <th>Memòria total</th>
                <th>Memòria disponible</th>
                <th>Memòria en us</th>
                <th>Percentatge</th>
        </tr>
        </thead>
<?php foreach ($db_connection->query($selectmemoria) as $row){ ?>
<tr>    
        <td><?php echo $row["Total"]?></td>
        <td><?php echo $row["Disponible"]?></td>
        <td><?php echo $row["En_us"]?></td>
        <td><?php echo $row["Percentatge"]?></td>
</tr>
<?php } ?>
</table>
</div>
<?php 
}

if ($info=='Disc') {
?>
<h1>Informació del disc</h1>
<div class="taula">
<table>
        <thead>
        <tr>
		<th>Dispositiu</th>
                <th>Punt de muntatge</th>
                <th>File System</th>
                <th>Espai total</th>
                <th>Espai utilitzat</th>
                <th>Espai disponible</th>
                <th>Percentatge</th>
        </tr>
        </thead>
<?php foreach ($db_connection->query($selectdisc) as $row){ ?>
<tr>    
	<td><?php echo $row["Dispositiu"]?></td>
        <td><?php echo $row["Punt_de_muntatge"]?></td>
        <td><?php echo $row["File_system"]?></td>
        <td><?php echo $row["Tamany_total"]?></td>
        <td><?php echo $row["Utilitzat"]?></td>
        <td><?php echo $row["Disponible"]?></td>
        <td><?php echo $row["Percentatge"]?></td>
</tr>
<?php } ?>
</table>
</div>
<?php 
}

if ($info=='Xarxa') {
?>
<h1>Informació de la xarxa</h1>
<div class="taula">
<table>
        <thead>
        <tr>
                <th>Interfície</th>
                <th>IP</th>
                <th>Mascara</th>
                <th>IP Broadcast</th>
        </tr>
        </thead>
<?php foreach ($db_connection->query($selectxarxa) as $row){ ?>
<tr>    
        <td><?php echo $row["Interficie"]?></td>
        <td><?php echo $row["IP"]?></td>
        <td><?php echo $row["Mascara"]?></td>
        <td><?php echo $row["Broadcast_ip"]?></td>
</tr>
<?php } ?>
</table>
</div>
<?php 
}



$resultdisc=$db_connection->query($selectdisc);
$resultmemoria=$db_connection->query($selectmemoria);
$resultsistema=$db_connection->query($selectsistema);
$resultxarxa=$db_connection->query($selectxarxa);

?>
</body>
</html>
```
